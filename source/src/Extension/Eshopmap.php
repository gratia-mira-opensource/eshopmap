<?php
namespace Joomla\Plugin\Task\Eshopmap\Extension;
/**
 * @package    eshopMap
 *
 * @author     Christoph J. Berger <w@gratia-mira.ch>
 * @copyright  Christoph J. Berger
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://gratia-mira.ch
 */


use Joomla\CMS\Factory;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\Component\Scheduler\Administrator\Event\ExecuteTaskEvent;
use Joomla\Component\Scheduler\Administrator\Task\Status as TaskStatus;
use Joomla\Component\Scheduler\Administrator\Traits\TaskPluginTrait;
use Joomla\Event\DispatcherInterface;
use Joomla\Event\SubscriberInterface;
use Joomla\Filesystem\Folder;
use Joomla\Filesystem\Path;
use LogicException;

// phpcs:disable PSR1.Files.SideEffects
\defined('_JEXEC') or die;
// phpcs:enable PSR1.Files.SideEffects

/**
 * Task plugin with routines that offer checks on files.
 * At the moment, offers a single routine to check and resize image files in a directory.
 *
 * @since  4.1.0
 */
final class Eshopmap extends CMSPlugin implements SubscriberInterface
{
	use TaskPluginTrait;

	/**
	 * @var string[]
	 *
	 * @since 4.1.0
	 */
	protected const TASKS_MAP = [
		'eshopmap.settings' => [
			'langConstPrefix' => 'PLG_TASK_ESHOP_MAP_TASK_MAP',
			'form'            => 'settings',
			'method'          => 'writeMap',
		],
	];

	/**
	 * @inheritDoc
	 *
	 * @return string[]
	 *
	 * @since 4.1.0
	 */
	public static function getSubscribedEvents(): array
	{
		return [
			'onTaskOptionsList'    => 'advertiseRoutines',
			'onExecuteTask'        => 'standardRoutineHandler',
			'onContentPrepareForm' => 'enhanceTaskItemForm',
		];
	}

	/**
	 * @var boolean
	 * @since 4.1.0
	 */
	protected $autoloadLanguage = true;

	/**
	 * The root directory path
	 *
	 * @var    string
	 * @since  4.2.0
	 */
	private $rootDirectory;

	/**
	 * Constructor.
	 *
	 * @param   DispatcherInterface  $dispatcher     The dispatcher
	 * @param   array                $config         An optional associative array of configuration settings
	 * @param   string               $rootDirectory  The root directory to look for images
	 *
	 * @since   4.2.0
	 */
	public function __construct(DispatcherInterface $dispatcher, array $config, string $rootDirectory)
	{
		parent::__construct($dispatcher, $config);
		$this->rootDirectory = $rootDirectory;

	}

	/**
	 * @param   ExecuteTaskEvent  $event  The onExecuteTask event
	 *
	 * @return integer  The exit code
	 *
	 * @throws \RuntimeException
	 * @throws LogicException
	 * @since 4.1.0
	 */
	protected function writeMap(ExecuteTaskEvent $event): int
	{

		$db = Factory::getContainer()->get('DatabaseDriver');
		$query = $db->getQuery(true);


//		$query->select('b.product_alias')
		$query->select('a.custom_fields,b.product_alias,b.product_name')
			->from('#__eshop_products AS a')
			->innerJoin('#__eshop_productdetails AS b ON a.id = b.product_id')
			// Nur aktive Produkte
			->where('a.published = 1')
			// Nur Produkte der aktuellen Sprache
			/** @todo Sprache aus Einstellungen übernehmen */
			->where('b.language LIKE "de-DE"')
			// Sortierung nach ABC ohne Anführungszeichen
			->order('b.product_id ASC');

		$db->setQuery($query);

		// Objekte laden
		$Produkte = $db->loadObjectList();

		$Links = '';
		$i = 0;
		$AuswahlDatenliste = [];
		$AuswahlAutoren = [];
		$URI = str_replace('administrator/','',Uri::base());
		foreach ($Produkte as $Produkt) {
			// Sitemap generieren
			$Links .= $URI ."online-store/" .  \GMF_Allgemein::umbenennenzuAliasname($Produkt->product_alias) . ".html\r\n";

			// Für Datalist vorbereiten & wird im Parameter als ein neuer Abschnitt beachtet
			$AuswahlDatenliste[$i] = str_replace('&','+',trim($Produkt->product_name));
			$Autor = json_decode($Produkt->custom_fields);
			$AuswahlAutoren[$i] = trim($Autor->field_autor);
			$i++;
		}
		file_put_contents($this->rootDirectory . "/EShop.txt", $Links);

		if(is_dir($this->rootDirectory . '/templates/druckfenster')) {
			asort($AuswahlDatenliste);
			asort($AuswahlAutoren);
			$AuswahlDatenliste = array_unique($AuswahlDatenliste);
			$AuswahlAutoren = array_unique($AuswahlAutoren);
			$Auswahl = array_merge($AuswahlAutoren,$AuswahlDatenliste);
			file_put_contents($this->rootDirectory . "/templates/druckfenster/html/mod_eshop_search/datalist.txt", serialize($Auswahl));
		}
		return TaskStatus::OK;
	}
}